## Instructions

## Initial setup
git clone https://eriverae@bitbucket.org/eriverae/ae-frontend-texteditor-reactjs-skeleton.git
##
cd ae-frontend-texteditor-reactjs-skeleton
##
Run `npm install` in order to setup application

## Development server
Run `npm start` for a dev server.
