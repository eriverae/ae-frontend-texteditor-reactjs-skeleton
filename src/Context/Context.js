import React from 'react';

// This is where the initial value i set
const Context = React.createContext({
    enableStyle: false,
    html: "",
    selectedWord: "",
    synonyms: [],
    handleDoubleClick() {},
    handleClick() {},
    insertWord() {},
    handleChange() {},
})

export const Provider = Context.Provider;
export const Consumer = Context.Consumer;
