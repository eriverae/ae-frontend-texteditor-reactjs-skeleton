import React, {Component} from 'react';
import './App.css';
import ControlPanel from "./control-panel/ControlPanel";
import FileZone from "./file-zone/FileZone";
import getMockText from './text.service';
import { Provider } from './Context/Context';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            enableStyle: false,
            selectedWord: "",
            html: "",
            synonyms: [],
            handleDoubleClick: this.handleDoubleClick,
            handleClick: this.handleClick,
            insertWord: this.insertWord,
            handleChange: this.handleChange,

        }
    }

    getSynonyms = word => {
        fetch(`https://api.datamuse.com/words?rel_syn=${word}`)
            .then(response => response.json())
            .then(data => this.setState({ synonyms: data}))
    }

    handleDoubleClick = () => {
        const selectedWord = document.getSelection().toString();
        this.setState({ enableStyle: true })
        this.setState({ selectedWord })
        this.getSynonyms(selectedWord)
    }

    handleClick = () => this.setState({ enableStyle: false })

    insertWord = word => {
        const { html, selectedWord } = this.state
        const newHtml = html.replace(selectedWord, word)
        this.setState({ html: newHtml })
    }

    handleChange = event => {
        this.setState({html: event.target.value});
    }

    getText() {
        getMockText().then(function (result) {
            console.log(result);
        });
    }
    render() {
        return (
            <Provider value={this.state}>
                <div className="App">
                    <header>
                        <span>Simple Text Editor</span>
                    </header>
                    <main>
                        <ControlPanel/>
                        <FileZone/>
                    </main>
                </div>
            </Provider>
        );
    }
}

export default App;
