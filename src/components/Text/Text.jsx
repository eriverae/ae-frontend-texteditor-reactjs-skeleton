import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './styles.css';

class Text extends Component {

    shouldComponentUpdate (nextProps){
        return nextProps.html !== ReactDOM.findDOMNode(this).innerHTML;
    }

    emitChange = () => {
        let html = ReactDOM.findDOMNode(this).innerHTML;
        if (this.props.onChange && html !== this.lastHtml) {

            this.props.onChange({
                target: {
                    value: html
                }
            });
        }
        this.lastHtml = html;
    }

    indentation = e => {
        if( e.keyCode === 9 ) {
            document.execCommand("indent", true, null);
        }
    }

    render() {
        return(
                <div
                    id="container"
                    onKeyDown={(e) => this.indentation(e)}
                    contentEditable
                    suppressContentEditableWarning
                    onDoubleClick={this.props.onDoubleClick}
                    onClick={this.props.onClick}
                    onInput={this.emitChange} 
                    onBlur={this.emitChange}
                    dangerouslySetInnerHTML={{__html: this.props.html}} >
                </div>
        )
    }
};

export default Text;
