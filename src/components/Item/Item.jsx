import React, { Component } from 'react';

class Item extends Component {
    render() {
        const { word, onClick } = this.props;
        return(
            <div onClick={() => onClick(word)}>
                {this.props.word}
            </div>
        )
    }
}

export default Item;
