import React, { Component } from 'react';
import Item from '../Item/Item';
import './styles.css';

class Synonyms extends Component {

renderItems = () => (
    this.props.synonyms.map((synonym, index) => (
            <Item 
                key={index}
                word={synonym.word}
                onClick={this.props.insertWord}
            />
    ))
)

    render() {
        return(
            <div 
                id="container"
            >
                {this.renderItems()}
            </div>
        );
    }
}


export default Synonyms;
