import React, { Component } from 'react';
import { Consumer } from '../Context/Context';
import './ControlPanel.css';

class ControlPanel extends Component {

    applyStyle = (style, enableStyle) => {
        const color = document.getElementById("getColor").value
        const varable = color ? color : null
        enableStyle && document.execCommand (style, false, varable)
    }

    render() {
        return (
            <Consumer>
                {context => (
                    <div id="control-panel">
                        <div id="format-actions">
                            <button
                                className="format-action"
                                type="button"
                                onClick={() => this.applyStyle('bold', context.enableStyle)}>
                                    <b>B</b>
                            </button>
                            <button 
                                className="format-action"
                                type="button"
                                onClick={() => this.applyStyle('italic', context.enableStyle)}>
                                    <i>I</i>
                            </button>
                            <button
                                className="format-action"
                                type="button"
                                onClick={() => this.applyStyle('underline', context.enableStyle)}>
                                    <u>U</u>
                            </button>
                            <input
                                className="color-actions"
                                type="color"
                                id="getColor"
                                onChange={() => this.applyStyle('forecolor', context.enableStyle)}
                                >
                            </input>
                        </div>
                    </div>
                )}
            </Consumer>
        );
    }
}

export default ControlPanel;
