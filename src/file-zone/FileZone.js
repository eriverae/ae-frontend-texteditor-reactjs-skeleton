import React, { Component } from 'react';
import Text from '../components/Text/Text';
import './FileZone.css';
import Synonyms from '../components/Synonyms/Synonyms';
import { Consumer } from '../Context/Context';

class FileZone extends Component {
    render() {
        return (
            <Consumer>
                {context => (   
                    <div id="file-zone">
                        <div id="file">
                            <Text 
                                onDoubleClick={context.handleDoubleClick}
                                onClick={context.handleClick}
                                html={context.html}
                                onChange={context.handleChange}
                            />
                        </div>
                        <div id="syns">
                            <Synonyms 
                                synonyms={context.synonyms}
                                insertWord={context.insertWord}
                            />
                        </div>
                    </div>
                )}
            </Consumer>
        );
    }
}

export default FileZone;
